import React from "react";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import './App.css'
import Navbar from './components/Navbar'
import Intro from './components/Intro'
import Resume from './components/Resume'
import MyForm from './components/MyForm'

function App() {


  return (

    <div className="App">
      <BrowserRouter>
      <Navbar />
      <Routes>
      <Route path="/me/" element={<Intro />} />
      <Route path="/me/resume" element={<Resume />} />
      <Route path="/me/contact" element={<MyForm />} />
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App
