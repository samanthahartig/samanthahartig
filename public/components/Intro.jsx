import React from 'react';
import { Link } from 'react-router-dom';
import img from "./headerimg.jpg";
import me from "./lightmode.jpg";
import grad from "./cap.png";
import bulb from "./lightbulb.png";
import job from "./job.png";
import dwg from "./dwg.png"

const Intro = () => {
    return (
        <div className="main-container">
            <div className="image-container">
                <img src={img} alt="Background" className="background-image" />
                <div className="text-samantha">
                    SAMANTHA
                </div>
            </div>
            <div className="text-hartig">
                HARTIG
            </div>
            <br></br><br></br>
            <div>
                <h2>Please excuse the rough layout- this is work in progress</h2>
            </div>
            <div className="flex-container">
            <div className="image-me">
                    <img src={me} alt="headshot" />
                </div>
                <div className="text-left">
                    <h3>Software Engineer</h3>
                    <p>See below for an overview of my story</p>
                    <p>See my <Link to="/me/resume">resume</Link> for full details of my experience</p>
                </div>
            </div>
            <div className="text-left">
                    <h3>Technical Skills</h3>
                    <p>Languages: Python, JavaScript, SQL, HTML5, CSS</p>
                    <p>Frontend: React, React Hooks, Redux Toolkit</p>
                    <p>Backend & Database: Django 4, PostgreSQL, MySQL, FastAPI</p>
                    <p>Tools & Systems: Insomnia, Docker, VMWare</p>
                    <p>Industrial Systems: PLC, SCADA, OIT, HMI</p>
                </div>
                <div className="text-left">
                    <h3>Soft Skills</h3>
                    <p>Project Management, Customer Support, Troubleshooting, Training, Cross-functional Collaboration, Technical Documentation</p>
                    </div>
            <table className="resume-table">
                <tbody>
                    <tr>
                        <td className="image-cell">
                            <img src={grad} alt="Grad Cap" />
                        </td>
                        <td className="text-cell">
                            B.S. Graphic Design
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={bulb} alt="lightbulb" />
                        </td>
                        <td className="text-cell">
                            Solar Car Team
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={job} alt="briefcase" />
                        </td>
                        <td className="text-cell">
                            Aviation Electrician's Mate
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={grad} alt="Grad Cap" />
                        </td>
                        <td className="text-cell">
                            A.A.S. Electrical Technologies
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={job} alt="briefcase" />
                        </td>
                        <td className="text-cell">
                            Engineering Technologist- PLC and HMI programming
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={job} alt="briefcase" />
                        </td>
                        <td className="text-cell">
                            Control Systems Engineer - SCADA and HMI design
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={bulb} alt="lightbulb" />
                        </td>
                        <td className="text-cell">
                            SCADA and HMI User interfaces combine engineering and graphic design
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={grad} alt="Grad Cap" />
                        </td>
                        <td className="text-cell">
                            Software Engineering Certificate
                        </td>
                    </tr>
                    <tr>
                        <td className="image-cell">
                            <img src={grad} alt="Grad Cap" />
                        </td>
                        <td className="text-cell">
                            coming soon: M.S. Software Engineering
                        </td>
                    </tr>
                </tbody>
            </table>
            <div>
            <img src={dwg} alt="new paltz outline" className="bottom-image" />
            </div>
        </div>
    );
};

export default Intro;
