import React, { useState } from 'react';

const FORM_URL = import.meta.env.VITE_REACT_APP_FORM_URL;

const MyForm = () => {
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [hasError, setHasError] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const formData = new FormData(event.target);
        try {
            const response = await fetch(FORM_URL, {
                method: 'POST',
                body: formData,
            });

            if (response.ok) {
                console.log("Form submitted successfully.");
                setIsSubmitted(true);
                // Redirect to home after 3 seconds
                setTimeout(() => {
                    window.location.href = '/';
                }, 3000);
            } else {
                console.error("Error submitting form:", response.statusText);
                setHasError(true);
            }
        } catch (error) {
            console.error("Submission error:", error);
            setHasError(true);
        }
    };

    if (isSubmitted) {
        return (
            <div style={{ padding: '20px', maxWidth: '600px', margin: '0 auto' }}>
                <h2>Thank You!</h2>
                <p>Your message has been sent successfully. Redirecting to the home page...</p>
            </div>
        );
    }

    if (hasError) {
        return (
            <div style={{ padding: '20px', maxWidth: '600px', margin: '0 auto' }}>
                <h2>Submission Error</h2>
                <p>There was an error submitting the form. You can reach out directly at <a href="mailto:samanthahartig@gmail.com">samanthahartig@gmail.com</a>.</p>
            </div>
        );
    }

    return (
        <div style={{ padding: '20px', maxWidth: '600px', margin: '0 auto' }}>
            <h2>Contact Me</h2>
            <form action={FORM_URL} method="POST" onSubmit={handleSubmit}>
                <div>
                    <label>Name:</label>
                    <input type="text" name="name" />
                </div>
                <div>
                    <label>Email:</label>
                    <input type="email" name="email" />
                </div>
                <div>
                    <label>Message:</label>
                    <input type="text" name="message" />
                </div>
                <input type="hidden" name="_gotcha" style={{ display: 'none' }} />
                <button type="submit">Send</button>
            </form>
        </div>
    );
};

export default MyForm;
