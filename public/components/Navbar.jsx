import React from 'react';
import linkedin from "./linkedin.png";
import gitlab from "./gitlab.png"

const Navbar = () => {
    return (
        <nav className="navbar">
            <ul className="navbar-list">
                <li className="navbar-item"><a href="/me/">HOME</a></li>
                <li className="navbar-item"><a href="/me/resume">RESUME</a></li>
                <li className="navbar-item"><a href="/me/contact">CONTACT</a></li>
                <li className="navbar-item icon">
                    <a href="https://gitlab.com/samanthahartig" target="_blank" rel="noopener noreferrer">
                        <img src={gitlab} alt="GitLab" />
                    </a>
                </li>
                <li className="navbar-item icon">
                    <a href="https://www.linkedin.com/in/hartigsa/" target="_blank" rel="noopener noreferrer">
                        <img src={linkedin} alt="LinkedIn" />
                    </a>
                </li>
            </ul>
        </nav>
    );
};

export default Navbar;
