const Resume = () => {
    return (
        <div style={{ fontFamily: 'Arial, sans-serif', margin: '20px', maxWidth: '800px' }}>
            <h1>Samantha Hartig</h1>
            <p>Greater Tampa Bay Area | samanthahartig@gmail.com | linkedin.com/in/hartigsa</p>

            <h2>Technical Skills</h2>
            <p>
                <strong>Languages:</strong> Python, JavaScript, SQL, HTML5, CSS<br />
                <strong>Frontend:</strong> React, React Hooks, Redux Toolkit<br />
                <strong>Backend & Database:</strong> Django 4, PostgreSQL, MySQL, FastAPI<br />
                <strong>Tools & Systems:</strong> Insomnia, Docker, VMWare<br />
                <strong>Industrial Systems:</strong> PLC, SCADA, OIT, HMI
            </p>

            <h2>Professional Experience</h2>
            <h3>Systems Engineer</h3>
            <p>General Control Systems, Inc | Mar 2018 – May 2023 | full-time</p>
            <p>
                <em>Engineering/QA:</em><br />
                • Designed user interfaces for real-time monitoring and data visualization, incorporating user feedback to emphasize UX, interactivity, and responsive design.<br />
                • Engineered data collection processes from multiple inputs, integrating front-end interfaces with back-end systems and databases, ensuring real-time data synchronization and system reliability. <br />
                • Interpreted system requirements from electrical and site drawings and technical specifications. <br />
                • Executed rigorous testing procedures, including FAT (Factory Acceptance Testing) and SAT (Site Acceptance Testing), to guarantee the delivery of high-quality projects. <br />
                <em>Project Management:</em><br />
                • Managed contracts up to $600K each, coordinating up to 6 projects simultaneously for timely delivery. <br />
                <em>Mentoring & Training: </em><br />
                • Led remote deployment solutions and provided phone support for junior engineers during on-site troubleshooting. <br />
                • Designed and authored training materials, leading the onboarding and education of new team members.<br />
                <em>Cross-functional Collaboration: </em><br />
                • Collaborated with clients to identify areas needing enhancement during routine maintenance, system upgrades, and other planned projects ensuring customer requirements were met.  <br />
                • Collaborated with sales and technical teams to grasp customer needs, authoring compelling project proposals valued up to $1M, which contributed significantly to business revenue.<br />
                <em>Documentation: </em><br />
                • Compiled project submittals and technical documentation, ensuring transparency and accurate record-keeping for stakeholders.<br />
                • Compiled bill of materials, datasheets, I/O lists. <br />

                <em>Continuous Learning:</em><br />
                • Engaged in continuous professional development, actively pursuing industry-related education and certifications, keeping up-to-date with the latest trends and technologies.
            </p>

            <h3>Engineering Technologist</h3>
            <p>Naval Nuclear Laboratory (FMP) | Jan 2015 – Mar 2018 | full-time</p>
            <p>
                • Developed and integrated software solutions for Schneider Electric PLCs and HMIs. <br />
                • Oversaw and updated lab software and electronics, focusing on maximizing system performance and uptime.  <br />
                • Orchestrated the acquisition and deployment of desktop hardware and software, supporting the technical needs of a 100+ member team. <br />

            </p>

            <h3>Aviation Electricians Mate</h3>
            <p>US Navy | Mar 2010 – Mar 2012 | full-time</p>
            <p>
            • Oversaw and ensured the reliability of complex electrical systems for EA-6B Prowler aircraft, emphasizing system diagnostics and timely maintenance.
            </p>

            <h2>Education</h2>
            <h3>West Virginia University</h3>
            <p>Masters of Science (M.S.), Software Engineering | 2025</p>

            <h3>SUNY New Paltz</h3>
            <p>Bachelor of Science (B.S.), Design and Visual Communications | 2009</p>

            <h3>Fulton-Montgomery Community College</h3>
            <p>Associate of Applied Science (A.A.S.) Electrical Technology | 2014</p>

            <h3>Galvanize Inc</h3>
            <p>774 hour certificate, Software Engineering | 2023</p>
        </div>
    );
};

export default Resume;
