- 👋 Hi, I’m Samantha
- 👀 I’m interested in engineering and design
- 🌱 I’m currently learning as much as I can. M.S. in Software Engineering in progress
- 📫 https://samanthahartig.gitlab.io/portfolio/
